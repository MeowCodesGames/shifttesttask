FROM python:3.11
RUN pip install poetry
COPY . /app
WORKDIR /app
RUN poetry install
RUN pip install fastapi-login
CMD ["poetry", "run", "uvicorn", "main:app", "--host", "127.0.0.1", "--port", "8000"]