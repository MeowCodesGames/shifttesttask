from fastapi import FastAPI, Depends, HTTPException, Request
from fastapi.security import HTTPBasic, HTTPBasicCredentials
from fastapi.staticfiles import StaticFiles
from fastapi.templating import Jinja2Templates
from fastapi_login import LoginManager
import secrets
from pydantic import BaseModel


app = FastAPI()
security = HTTPBasic()
manager = LoginManager("key", "/login")

app.mount("/static", StaticFiles(directory="static"), name="static")
templates = Jinja2Templates(directory="templates")


employee_db = [
    {"id": 1, "username": "Alex", "password": "alex123", "salary": 50000, "next_promotion": "30-07-2023", "token": ""},
    {"id": 2, "username": "Maya", "password": "maya123", "salary": 60000, "next_promotion": "7-07-2023", "token": ""},
    {"id": 3, "username": "Vic", "password": "vic123", "salary": 90000, "next_promotion": "25-07-2023", "token": ""},
    {"id": 4, "username": "Tanya", "password": "tanya123", "salary": 150000, "next_promotion": "3-08-2023", "token": ""},
]


class Item(BaseModel):
    name: str
    description: str


def check_credentials(credentials: HTTPBasicCredentials):
    for user in employee_db:
        if user['username'] == credentials.username and user['password'] == credentials.password:
            return user
    raise HTTPException(status_code=401, detail="Invalid username or password")


def generate_new_token():
    token = secrets.token_hex(16)
    return token


@app.get("/")
def home(request: Request):
    return templates.TemplateResponse("home.html", {"request": request})


@app.post("/login")
async def login(request: Request, credentials: HTTPBasicCredentials = Depends(security)):
    employee = check_credentials(credentials)
    access_token = generate_new_token()
    employee["token"] = access_token
    return templates.TemplateResponse("token.html", {"request": request, "access_token": access_token})


@app.get("/salary")
def salary(request: Request, username: str, token: str):
    employee = None
    for emp in employee_db:
        if emp["username"] == username:
            employee = emp
            break

    if not employee:
        raise HTTPException(status_code=404, detail="Employee not found")

    if token != employee["token"]:
        raise HTTPException(status_code=401, detail="Invalid token")

    return templates.TemplateResponse("salary.html",
                                      {"request": request, "salary": employee["salary"],
                                       "next_promotion": employee["next_promotion"]})
